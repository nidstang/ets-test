import moment from 'moment'
import Config from 'config'

export default (timestamp) => {
  return moment(timestamp).format(Config.date.format)
}
