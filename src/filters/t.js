export default (i18n) => {
  return key => i18n.t(key)
}
