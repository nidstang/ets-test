export default (str, maxLength) => {
  return str.slice(0, maxLength).concat('...')
}
