export default (bool) => {
  return bool ? 'yes' : 'no'
}
