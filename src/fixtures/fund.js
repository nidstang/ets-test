export default {
  'id': 9366,
  'name': 'Stable Return Fund',
  'isin': null,
  'currency': {
    'id': 1,
    'name': 'EUR',
    'symbol': '€',
    'color': '#31319C'
  }
}
