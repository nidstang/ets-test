export default {
  'id': 78178,
  'name': 'Stable Return Fund BP Acc',
  'isin': 'LU0227384020',
  'currency': {
    'id': 1,
    'name': 'JPY',
    'symbol': '¥',
    'color': '#31319C'
  },
  'symbol_type': 'FundClass',
  'region': {
    'id': 900000,
    'name': 'Global',
    'region_level2': {
      'id': 901000,
      'name': 'Global',
      'region_level3': {
        'id': 901095,
        'name': 'Global'
      }
    }
  },
  'sector': {
    'id': 95000000,
    'name': 'No sector',
    'sector_level2': null
  },
  'risk_family': {
    'id': 3000,
    'name': 'Balanced',
    'sub_family': {
      'id': 3015,
      'name': 'Absolute Return'
    }
  },
  'issuer': {
    'id': 3060,
    'name': 'Nordea'
  },
  'is_hedged': false,
  'fund': {
    'id': 9366,
    'name': 'Stable Return Fund',
    'isin': null,
    'currency': {
      'id': 1,
      'name': 'EUR',
      'symbol': '€',
      'color': '#31319C'
    }
  }
}
