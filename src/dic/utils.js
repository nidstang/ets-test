export const getDependencyByName = (container) => {
  return name => {
    return container[name]
  }
}
