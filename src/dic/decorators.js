import di from './di'
import { getDependencyByName } from './utils'

export const Service = (name, ...dependencies) => {
  return (Target, property, descriiptor) => {
    di.factory(name, container => {
      return new Target(...(dependencies.map(getDependencyByName(container))))
    })
  }
}
