import Bottle from 'bottlejs'

export default Bottle.pop('application')
