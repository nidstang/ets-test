import { Service } from '@/dic/decorators'
import { Root } from '@/utils/decorators'

@Service('api', 'http')
class Api {
  constructor (http) {
    this.http = http
  }

  @Root()
  get (url) {
    // { data: {} } JsonStub issue. More info: https://stackoverflow.com/a/42992011
    return this.http.get(url, { data: {} }).then(response => response.data)
  }
}

export default Api
