import { Service } from '@/dic/decorators'

@Service('logger', 'debug')
class Logger {
  constructor (debug) {
    this.debug = debug
  }

  _print (msg, type) {
    if (this.debug) {
      console[type](msg)
    }
  }

  info (msg) {
    this._print(msg, 'info')
  }

  error (msg) {
    this._print(msg, 'error')
  }

  warn (msg) {
    this._print(msg, 'warn')
  }
}

export default Logger
