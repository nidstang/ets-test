import { Service } from '@/dic/decorators'

@Service('vault', 'logger')
class Vault {
  constructor (logger) {
    this.logger = logger
    this.local = window.localStorage
  }

  isAvailable () {
    return window.localStorage !== null
  }

  get (key) {
    this.local.getItem(key)
  }

  set (key, value) {
    this.local.setItem(key, value)
  }

  // create a new vault if does not exist and it returns it
  create (id) {
    const vault = this.get(id)
    if (!vault) {
      this.local.setItem(id, JSON.stringify([]))
      return []
    }

    return vault
  }

  get (id) {
    const vault = this.local.getItem(id)
    return vault ? JSON.parse(vault) : null
  }

  save (id, vault) {
    if (vault instanceof Array) {
      this.local.setItem(id, JSON.stringify(vault))
    }
  }

  findObject (idVault, criteria) {
    const collection = this.get(idVault)
    if (collection) {
      return collection.find(obj => obj[criteria.key] === criteria.value)
    }

    return null
  }
}

export default Vault
