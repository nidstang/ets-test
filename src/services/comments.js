import { Service } from '@/dic/decorators'

@Service('comments', 'vault')
class Comments {
  constructor (vault) {
    this.vault = vault
    this.id = Number(vault.get('nextId')) || 0
  }

  // save a comment in a vault
  saveComment (id, commentText) {
    const vault = this.vault.create(id)
    const now = new Date().getTime()
    vault.push({ id: this.id++, content: commentText, timestamp: now })
    this.vault.set('nextId', this.id)
    this.vault.save(id, vault)
  }

  // get all comments list from a vault id
  getAll (id) {
    const vault = this.vault.get(id)
    return vault || []
  }

  // get count from a vault id
  getCount (id) {
    const vault = this.getAll(id)
    return vault.length
  }

  // delete a comment by create a new list without that comment
  deleteComment (vaultId, commentId) {
    let vault = this.vault.get(vaultId)
    if (vault) {
      vault = vault.filter(comment => comment.id !== commentId)
      this.vault.save(vaultId, vault)
    }
  }
  
  // modify a comment from its vaultid and commentid
  modifyComment (vaultId, commentId, newContent) {
    let vault = this.vault.get(vaultId)
    if (vault) {
      const comment = vault.find(comment => comment.id === commentId)
      comment.content = newContent
      this.vault.save(vaultId, vault)
    }
  }
}

export default Comments
