import dic from '@/dic/di'
import Axios from 'axios'
import Config from 'config'

dic.factory('http', _ => {
  const axios = Axios.create()
  const defaults = axios.defaults
  axios.defaults.headers = { ...defaults.headers, ...Config.http.headers }
  return axios
})
