export default {
  yes: 'Si',
  no: 'No',
  languages: {
    en: 'Inglés',
    es: 'Español'
  },
  generals: {
    name: 'Nombre',
    yourActives: 'Tus activos',
    update: 'Actualizar',
    delete: 'Borrar',
    create: 'Crear',
    goBack: 'Atrás',
    typeHere: 'Escribe aquí...',
    loadingActive: 'Cargando activo'
  },
  messages: {
    noData: 'Nada para mostrar',
    noActives: 'No tienes activos',
    noActivesFiltered: 'No tienes activos con ese criterio de busqueda',
    noDetails: 'Selecciona un activo para ver mas información'
  },
  symbols: {
    currency: 'Divisa',
    symbol: 'Símbolo',
    symbolType: 'Tipo de símbolo',
    region: 'Región',
    sector: 'Sector',
    riskFamily: 'Familia de riesgo',
    issuer: 'Emisor',
    isHedged: '¿Está cubierto?',
    fund: 'Fondo',
    comments: 'Comentarios'
  },

  filters: {
    filters: 'Filtros',
    allCurrencies: 'Todas las divisias',
    allFamilies: 'Todas las familias'
  },

  comments: {
    noComments: 'No hay comentarios',
    showComments: 'Ver comentarios',
    create: 'Crear un nuevo comentario',
    commentCreated: 'Nuevo comentario creado',
    commentUpdated: 'Comentario actualizado',
    commentDeleted: 'Comentario borrado',
    help: 'Puedes editar un comentario haciendo click en el mismo y escribiendo el nuevo texto. Despues haz click en Actualizar para guardar los cambios'
  },

  chart: {
    closingPrices: 'Precios de cierre',
    title: 'Grafica temporal de precios'
  },

  help: 'Bienvenido a Actives portal. Haz click en uno de tus activos para ver información detallada sobre el mismo'
}
