export default {
  yes: 'Yes',
  no: 'No',
  languages: {
    en: 'English',
    es: 'Spanish'
  },
  generals: {
    name: 'Name',
    yourActives: 'Your actives',
    update: 'Updade',
    delete: 'Delete',
    create: 'New',
    goBack: 'Back',
    typeHere: 'Type here...',
    loadingActive: 'Loading active'
  },
  messages: {
    noData: 'Nothing to show',
    noActives: 'You do not have any active',
    noActivesFiltered: 'You do not have any active with that criteria',
    noDetails: 'Select one of your actives to show more info'
  },
  symbols: {
    currency: 'Currency',
    symbol: 'Symbol',
    symbolType: 'Symbol type',
    region: 'Region',
    sector: 'Sector',
    riskFamily: 'Risk family',
    issuer: 'Issuer',
    isHedged: 'Is it hedged?',
    fund: 'Fund',
    comments: 'Comments'
  },

  filters: {
    filters: 'Filters',
    allCurrencies: 'All currencies',
    allFamilies: 'All families'
  },

  comments: {
    noComments: 'There are not comments yet',
    showComments: 'Show comments',
    create: 'Create a new comment',
    commentCreated: 'A new comment was created',
    commentUpdated: 'A comment was updated',
    commentDeleted: 'Comment deleted successfuly',
    help: 'You can edit a comment by clicking in the comment box and writing your new changes. You need to click in Update to save your changes'
  },

  chart: {
    closingPrices: 'Closing prices',
    title: 'Temporal prices chart'
  },

  help: 'Welcome to Actives portal. Click at an Active to see more information'
}
