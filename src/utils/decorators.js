import Config from 'config'

export const Root = () => {
  return (target, property, descriptor) => {
    const origin = descriptor.value
    descriptor.value = function (url, ...params) {
      return origin.call(this, ...[`${Config.apiRoot}/${url}`, ...params])
    }
  }
}
