import Vue from 'vue'
import di from '@/dic/di'
import '@/dic/init'
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'
// routes
import routes from '@/router/routes'
// languages
import es from '@/lang/es'
import en from '@/lang/en'
// lang filters
import tFilter from '@/filters/t'
import App from './App'

// Register plugins
// I18n
Vue.use(VueI18n)
const i18n = new VueI18n({ locale: 'es', messages: { es, en } })

// Router
Vue.use(VueRouter)
const router = new VueRouter({ routes })

// Register filtes
Vue.filter('t', tFilter(i18n))

// Inject dic in Vue
Vue.prototype.$dic = di.container

const app = new Vue({
  el: '#app',
  i18n,
  router,
  render: h => h(App)
})
