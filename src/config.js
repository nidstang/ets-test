export default {
  date: {
    format: 'D/M/Y H:mm:ss'
  },
  chart: {
    borderColor: '#2A8911'
  },
  apiRoot: 'http://jsonstub.com/etsfintech',
  http: {
    headers: {
      'JsonStub-User-Key': '9facef2e-9583-4a83-9f08-c87159f1c113',
      'JsonStub-Project-Key': '6ed070c1-b334-4612-8fa8-169c5e45baef',
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    }
  }
}
