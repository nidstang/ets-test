const Details = () => import('@/components/Details')
const Comments = () => import('@/components/Comments')

export default [
  {
    name: 'details',
    path: '/details/:activeId',
    component: Details
  },
  {
    name: 'comments',
    path: '/comments/:activeId',
    component: Comments
  }
]
