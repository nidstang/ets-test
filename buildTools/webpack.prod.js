const merge = require('webpack-merge')
const webpack = require('webpack')
const baseConfig = require('./webpack.base')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')

module.exports = merge(baseConfig, {
  mode: 'production',

  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.runtime.min.js'
    }
  },

  plugins: [
    // ignore all moment locale except en
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/),
    new CompressionPlugin({
      test: /\.(js|css)$/
    })
  ],

  optimization: {
    minimizer: [
      new UglifyJsPlugin()
    ]
  }
})
