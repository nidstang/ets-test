# Instalacción y puesta en marcha
## Instalando
El proyecto hace uso de npm como gestor de dependencias. Por tanto se necesita tenerlo instalado primero.

Haciendo uso de npm, en la raiz de proyecto ejecutar:

``` npm i ```

## Lanzando el servidor de desarrollo
El proyecto viene con webpack-dev-server, que se encarga de transpilar el codigo y servirlo en un servidor http. El servidor se lanza en host:port. Por defecto se montará en http://localhost:8080 pero se puede cambiar esta configuración:

```
export APP_HOST=0.0.0.0
export APP_PORT=8081
```

Para lanzar el dev server, ejecutar:

```
npm run dev
```

## Generando versión para producción
### Manual
El proyecto está configurado y optimizado para su puesta en producción. En este mismo documento se explica el proceso de optimización seguido.

Para generar los ficheros de producción, ejecutar:

```
npm run prod
```

Los static assets generados se encontrarán en /dist y se necesitará de un servidor para servir dichos estaticos para su puesta en producción.

### Docker
He dispuesto una imagen de docker, donde un servidor hecho en nodejs + express se encarga de servir la aplicación en el puerto 80.
Está imagen se encuentra en:

[Docker image](https://hub.docker.com/r/nidstang/ets-test-server/)

### Online version
He dispuesto un servidor donde he desplegado la imagen de docker anteriormente citada, se encuentra en: [pfranco.tk](pfranco.tk)



## Dependencias
A continuación se muestra la lista de las depencencias de producción seleccionadas para el proyecto y los motivos de su elección:

1. Vue: Progressive Javascript Framework.
2. Vue-router: Para dotar de navegacion al SPA.
3. Vue-i18n: Para internacionalización. En este caso English y Spanish.
4. Axios: HTTP client basado en promesas y con un sistema de control de errores mejor que el que viene con **fetch**.
5. Bottle.js: Una libreria liviana que proporciona un contenedor de injección de dependencias.
6. Chart.js: Libreria para la creación de graficos funcionales y con buen aspecto visual.
7. Momment.js: Manejo y formateo de fechas. Es dependencia obligatoria de **Chart.js** y es usada en el proyecto para el correcto formateo de la fecha de creación en los comentarios.
8. Normalize.css: Pequeño snippet para la normalización de los estilos por defecto en los navegadores.

## Estructura del proyecto
En este punto he definido la estructura de ficheros y directorios de la forma que personalmente mejor suelo trabajar con Vue. Normalmente hago una división entre Views y Components, pero por la naturaleza del proyecto y al tratarse de algo pequeño he oobviado esta diferenciación y solo existe una unica carpeta de components.

A continuación muestro el arbol de directorios y una breve explicación:

```
/buildTools => Contiene las distintas configuraciones de Webpack
/dist => ficheros generados de la transpilación y empaquetado
/node_moudles => dependencias
/src
	/components => Vue components
	/dic => Todo lo referente al IoC
	/filtes => Mis custom filters para Vue
	/fixtures => Datos de prueba para tests
	/lang
		es.js => contiene spanish strings
		en.js => contiene english strings
	/router
		routes.js => define las rutas de la aplicación
	/sass => contiene mis snippets para sass
	/services => aquí creo los servicios que se registraran en el IoC
	/utils => pequeñas funciones con utilidades varias
	App.vue => aplicacion principal de Vue
	config.js => configuración del proyecto
	main.js => entrypoint de la aplicación
.babelrc => configuración de babel
.eslintrc => configuración de ESlint
index.template.html => html template usado por html-webpack-plugin
```

## Configuración
Es posible configurar diversos aspectos de la aplicación mediante el fichero src/config.js. El objeto de configuración por defecto es el siguiente:

```
{
 date: {
    format: 'D/M/Y H:mm:ss'
  },
  chart: {
    borderColor: '#2A8911'
  },
  apiRoot: 'http://jsonstub.com/etsfintech',
  http: {
    headers: {
      'JsonStub-User-Key': '9facef2e-9583-4a83-9f08-c87159f1c113',
      'JsonStub-Project-Key': '6ed070c1-b334-4612-8fa8-169c5e45baef',
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    }
  }
}
```

## Estilos y Layout
Por la naturaleza del proyecto en el cual he querido anteponer la correcta funcionalidad y optimización de la web al diseño, he decidido hacer uso de las herramientas que CSS3 proporciona de forma nativa para la creación de los **estilos y el layout**.

### CSS Grid
Debido a la buena compatibilidad entre navegadores de escritorio y móviles he elegido Css Grid como la mejor opción para crear todo el *layout* de la web. Ádemas no habia tenido la oportunidad real de usuarlo en un proyecto y me ha parecido una buena oportunidad de aprenderlo.

El layout está definido como 2 columnas y 2 filas en la versión de escritorio y 1 columna y 2 filas en dispositivos móviles (< 800px width):

- Desktop devices:

| Header        |     Header       |
| ------------- |:----------------:|
| Actives       | Details/Comments |

- Mobile devices:

| Header        |     Header       |
| ------------- |:----------------:|
| Actives       | Actives          |

(Details and comments son superpuestas encima de toda la aplicación)


## Componentes
Para la creación del proyecto (portal de activos) se han ido creando una serie de componentes con el objetivo de proporcionar mecanismos resusables a la hora de crear las distintas vistas de las que se compone la aplicación. A continuación procedo a listar y brevemente explicar cada uno de ellos:

- **ActiveCard**: Representa graficamente a un Activo y encapsula su funcionalidad.
- **Actives**: Componente que contiene y gestiona los componentes ActiveCards y el ActivesFilter.
- **ActivesFilter**: Encapsula de mostrado y selección de filtros
- **Comment**: Representa un comentario y ecapsula las funcionalidades de editarse y eliminarse a si mismo.
- **Comments**: Auna los componentes de Comment (en una lista) y CreateComment.
- **CreateComment**: Se encarga de proporcionar un mecanismo visual al usuario para crear un nuevo comentario.
- **Currency**: Visualiza una divisa tomando el objeto con el formato esperado en el servicio, que se compone de: id, name, symbol and color.
- **Details**: Es el componente mas complejo de la aplicación ya que contiene: Currency, Level, Fund, TimePriceChart and Loader.
- **Errors**: Sencillo componente para mostrar errores en una lista no-ordernada.
- **Fund**: Visualiza de la forma solicitada un Fondo que se ha de componer de: Currency and ISIN.
- **Header**: Representa la cabecera de la aplicacion con el nombre de la misma y el selector de lenguaje.
- **Level**: Procesa el objeto recibido desde el servicio y lo presenta en el formato especificado (separados por /).
- **Loader**: Un pequeño componente que se encarga de mostrar un loader animado y un texto configurable.
- **Message**: Muestra un mensaje encerrado en u parrafo y con un color predefinido.
- **TimePriceChart: Encapsula el mostrado del grafico temporal de precios haciendo uso de la libreria **Chart.js**. Algunas opciones del grafico son configurables a tráves de config.js.

Todos estos componentes no definen toda la funcionalidad. La mayoria utilizan servicios de la aplicación contenidos en el contenedor de inyección de dependencias y del que se habla en el siguiente apartado.

**Nota**: *En Vue y en otros frameworks del mismo tipo no se hace una diferenciación real entre Vista y Componente, por tanto este proyecto tampoco establece esa diferenciación. Personalmente a mi me gusta separar las vistas (definidas como contenedores de otros componentes o comunmente los componentes especificos de cada Ruta) de los componentes propiamente dichos.*

## Inyección de depencencias
Con el objetivo de no acoplar los componentes a tediosas dependencias mediante el sistema convencional de imports y ádemas proporcionar una forma sencilla de 'mockear' componentes, he decido utilizar el patrón de Inyección de depencencias y su implementación mas sencilla a tráves de un contenedor de IoC. Este contendor me lo proporciona Bottle.js y por mi parte solo he creado algunos helpers para hacer mas sencilla el registro de Servicios o Factorias.

### @Service
Hay una propuesta en el tc39 para tener @decoradores al mas estilo de python en las futuras especificaciones. Mientras tanto es posible usarlos a traves de un polyfill de babel proporcionado por el plugin trasnform-decorators-legacy.
De esta forma he podido implementar un decorador para el registro de servicios e injección de sus dependencias en un mismo lugar.

Todos las clases definidas en la carpeta services/ son registrados mediante este decorador

## Tests
La aplicación cuenta con un entorno de tests ejecutable desde npm test. Por falta de tiempo, solo se ha configurado el entorno de testeo haciendo uso de Mocha, Chai y Sinon e implementados algunos tests básicos ayudandome de @vue/test-utils. De esta forma se deja la puerta abierta a continuar testeando los componentes.

## Optimización para producción
En este apartado explicaré de forma general las decisiones que he tomado para configurar el proyecto para entornos de producción.

### Vue runtime
Como estoy usano los files components de vue, los componentes son compilados en el proceso de empaquetado mediante el vue-loader de webpack, por tanto no es necesario injectar este compilador en el bundle final. Haciendo uso de 'vue.runtime.min.js' nos libraremos de él y se reducirá un poco el tamaño del bundle.

### Momentjs locales
Momentjs es dependencia obligatoria de Chartjs y es una de las mejores librerias para hacer con las fechas todo lo que el Date de la standard no nos proporciona. Pero no hace falta empaquetar todos los locales que trae. Para este proyecto me sirve el de Inglés.

### SplitChunks
Utilizo la configuración de optimización que viene con Webpack 4 para splitear el bundle final según como haya utilizado los imports en el proyecto. Utilizo la estrategia de 'all', ya que parece ser la mejor opcion según muchos desarrolladores de webpack en github. Esta estrategia prioriza dynamic and static imports por igual.

### Route dynamic imports
Siguiendo la configuración del apartado anterior he decidido usar los dynamic imports de forma inteligente.
No tiene sentido cargar los componentes y subcomponetes Details.vue y Comments.vue hasta que no vaya a ir a esas rutas. Por tanto, estos componentes son cargados dinamicamente y entonces webpack sabe como splitear el bundle en consecuencia.

### Webpack compress text
Los estaticos están siendo comprimidos en gzip. Para aplicar esta mejora es necesario que el servidor que se encargue de servir los ficheros estaticos, esté configurado para tal fin y envie las cabeceras correspondientes.

### Uglify
Todo el codigo Javascript es minimizado mediante UglifyJsPlugin.


### Resultado

| Asset              |     Size         |
| ------------------ |:----------------:|
| 0.hash.js          | 57.1 KiB         |
| main.hash.js       | 30.5 KiB         |
| 2.hash.js          | 8.44 KiB         |
| 3.hash.js          | 8.36 KiB         |
| vendor.hash.js     | 159 KiB          |
| index.html         | 442 bytes        |

## TODO
Por la naturaleza del proyecto y debido al poco tiempo que disponia, se han quedado cosas que me hubiera gustado mejorar y/o implementar. Tales como:

- Mejor sistema de notificaciones, con el fin de proporcionarle al usuario el feedback correcto.
- Design mas atractivo.
- Optimizaciones del lado del servidor.
- Tests de todos los componentes.
- Paginacion de registros (no se ha implementado porque el api no está paginada).
- Optimización del sistema de comentarios.
- Recordar el idioma elegido por el usuario
- Autentificación (con JWT por ejemplo)

## Bugs encontrados
Hay algunos bugs que han ido apareciendo y que tengo identificados, pero que no me da tiempo a arreglar:

- Cuando se cambia de portrait a landscape en tablets (como el Ipad) la pantalla de detalles no ocupa todo el espacio del viewport.
- Existe un warning de Momentjs debido a como Charjs está esperando que se le pasen los labels. Intenta parsear la fecha, pero debido a querer hacer el grafico mas compacto mostrando solo el year de las fechas, solo le estoy pasando un numero.
- El grafico no toma los cambios del idioma, porque necesita repintarlo para tal fin.:w
