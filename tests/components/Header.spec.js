import { shallowMount } from '@vue/test-utils'
import HeaderComponent from '@/components/Header.vue'

describe('Header.vue', () => {
  const $i18n = { locale: 'es' }
  const wrapper = shallowMount(HeaderComponent, {
    mocks: { $i18n }
  })
  
  it ('Header title must be correct', () => {
    expect(wrapper.find('p').text()).to.equal('Actives portal')
  })
  
  it ('Header select must exist', () => {
    expect(wrapper.find('select').exists()).to.equal(true)
  })

  it ('Header select must have two locale options', () => {
    expect(wrapper.findAll('option')).to.have.lengthOf(2)
  })
})
