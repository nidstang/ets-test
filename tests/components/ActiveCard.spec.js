import { shallowMount } from '@vue/test-utils'
import ActiveCard from '@/components/ActiveCard'

const propsData = { 
  id: 1,
  name: 'test', 
  currency: 'EUR', 
  risk_family: 'Balanced',
  isActivate: true
}

describe('ActiveCard.vue', () => {
  const $dic = { 
    bus: {
      $on: sinon.spy()
    }, 
    comments: {
      getCount: sinon.spy()
    }
  }
  const w = shallowMount(ActiveCard, { propsData, mocks: { $dic } })
  w.setData({ comments: 3 })

  it ('Correct render', () => {
    expect(w.find('.card-title').text()).to.equal('test...')
    expect(w.find('.card-currency').text()).to.equal('EUR')
    expect(w.find('.card-info-family').text()).to.equal('Balanced')
    expect(w.find('.card-info-comments').text()).to.equal('3')
  })

  it ('comments.getCount must be called', () => {
    expect($dic.comments.getCount.calledOnce).to.equal(true)
  })

  it ('bus.$on must be called', () => {
    expect($dic.bus.$on.calledOnce).to.equal(true)
  })

  it ('bus.$on must be called with correct args', () => {
    expect($dic.bus.$on.calledOnceWith('updateComments')).to.equal(true)
  })
})